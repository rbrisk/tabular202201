#!/usr/bin/env python

from glob import glob
import kaggle
import os
import os.path as osp
import pandas as pd
import re
from zipfile import ZipFile

competition = "tabular-playground-series-jan-2022"
path_data = "data"
path_zip = osp.join(path_data, f"{competition}.zip")

if not osp.exists(path_zip):
	print("Downloading files")
	kaggle.api.authenticate()
	kaggle.api.competition_download_files(competition, path=path_data)

print("Extracting data")
with ZipFile(path_zip, "r") as ziph:
	ziph.extractall(path_data)
#os.unlink(path_zip)

# Convert to feather for faster io
for csv in glob(osp.join(path_data, "*.csv")):
	ftr = re.sub("csv$", "ftr", csv)
	print(f"Converting {csv} to {ftr}")
	if re.search("sample_submission", csv):
		ds = pd.read_csv(csv)
	else:
		ds = pd.read_csv(csv, parse_dates=["date"])
	ds.to_feather(ftr)
#	os.unlink(csv)
